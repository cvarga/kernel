#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>

#define __NR_get_zone_info  342

int main(int argc, char *argv[])
{
        syscall(__NR_get_zone_info,1);
        return 0;
}
