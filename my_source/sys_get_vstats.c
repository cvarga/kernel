#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/mm_types.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/path.h>
#include <linux/dcache.h>
asmlinkage int sys_get_vstats(int pid)
{

        // declare variables
        unsigned long total = 0;
        unsigned long size  = 0;

        // set a pointer to the task_struct with given pid
        struct task_struct *task;
        task = get_pid_task(find_get_pid(pid), PIDTYPE_PID);

        // set a pointer to VMA list
        struct vm_area_struct *curr;
        curr = task->mm->mmap;

        // print info
        printk("\n\n\n\n\n\n\n\n");
        printk("the get_vstats system call was made.\n\n");
        printk("size       permissions       file\n");

        while (curr->vm_next != NULL) {

            // get size of this VMA
            size = curr->vm_end - curr->vm_start;

            // print info
            printk("%-20lu %lu\n", size, (unsigned long)(curr->vm_page_prot.pgprot));
            total = total + size;

            // point to next VMA
            curr = curr->vm_next;
        }

        // print total size
        printk("total: %lu\n", total);

        return 1;
}
