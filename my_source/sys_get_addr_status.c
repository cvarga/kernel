#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/mm_types.h>
#include <linux/module.h>
#include <asm/pgtable.h>
#include <asm/page.h>

asmlinkage int sys_get_addr_status(unsigned int addr)
{
        // print syscall info
        printk("\n\n\n\n\n\n\n\n");
        printk("the get_addr_status system call was made.\n\n");

        // determine if address is in memory or on disk
        if (pmd_present(__pmd(addr)) == 1) {
            printk("address is in memory\n");
        } else {
            printk("address is on disk\n");
        }

        // convert address to pte_t for pte_young and pte_dirty
        __pte(addr);

        // determine if the page which the address belongs to has been
        // referenced or not
        if (pte_young(__pte(addr)) == 1) {
            printk("the page for this address has been accessed\n");
        } else {
            printk("the page for this address has not been accessed\n");
        }

        // determine if the page which this address belongs to is dirty
        if (pte_dirty(__pte(addr)) == 1) {
            printk("the page for this address is dirty\n");
        } else {
            printk("the page for this address is not dirty\n");
        }

        return 1;
}
