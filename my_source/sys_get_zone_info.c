#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/mm_types.h>
#include <linux/module.h>
#include <asm/pgtable.h>
#include <asm/page.h>

asmlinkage int sys_get_zone_info(unsigned int addr)
{
        // print syscall info
        printk("\n\n\n\n\n\n\n\n");
        printk("the get_zone_info system call was made.\n\n");

        struct zone *z;

        int numActivePages = 0;
        int numInactivePages = 0;
        for_each_zone(z) {
            //numActivePages += z->lr->vm_stat[NR_FREE_PAGES];
            //numInactivePages += z->lr->vm_stat[NR_FREE_PAGES];
        }

        printk("number of pages in active list: %d\n", numActivePages);
        printk("number of pages in inactive list: %d\n", numInactivePages);

        return 1;
}
