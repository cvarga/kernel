#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/module.h>

static const struct sched_class wrr_sched_class;

asmlinkage int set_wrr_scheduler(int pid, int weight)
{

        // set a pointer to the task_struct with given pid
        struct task_struct *task;
        task = get_pid_task(find_get_pid(pid), PIDTYPE_PID);

        // set the sched_class and the weight of the task
        task->sched_class = &wrr_sched_class;
        task->wrr->weight = weight;

        return 1;

}
