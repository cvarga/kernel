#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/pid.h>
asmlinkage int sys_print_other(int pid)
{
        // set a pointer to the task_struct with given pid
        struct task_struct *task;
        task = get_pid_task(find_get_pid(pid), PIDTYPE_PID);

        // print the given process' id, name, running state, start time, and virtual runtime
        printk("\n\n\n\n\n\n\n\n");
        printk("the print_other system call was made.\n\n");
        printk("current process info:\n--\n");
        printk("task->pid => %d\n", task->pid);
        printk("task->comm => %s\n", task->comm);
        printk("task->state => %ld\n", task->state);

        printk("\n\n(I am printing multiple 'start times' to cover all my bases.)\n--\n");
        printk("task->se.vruntime => %llu\n", task->se.vruntime);
        printk("task->se.exec_start => %llu\n", task->se.exec_start);
        printk("task->start_time.tv_sec => %ld\n", task->start_time.tv_sec);
        printk("task->real_start_time.tv_sec => %ld\n", task->real_start_time.tv_sec);
        printk("\n\n");

        // print the parent processes of the given process until init
        printk("parent processes until init:\n--\n");
        for(; task!=&init_task; task=task->parent) {
                printk("name: %s, pid: %d\n", task->comm, task->pid);
        }

        return 1;
}
