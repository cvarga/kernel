#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/pid.h>
#include <linux/module.h>
asmlinkage int get_wrr_weight(int pid)
{

        // set a pointer to the task_struct with given pid
        struct task_struct *task;
        task = get_pid_task(find_get_pid(pid), PIDTYPE_PID);

        // return the weight of the task
        return task->wrr->weight;

}
