/*
 * Weighted Round Robin Scheduling Class (mapped to the SCHED_WRR
 * policy)
 */


// prototypes
static inline int on_wrr_rq(struct sched_wrr_entity *wrr_se);
static void enqueue_task_wrr(struct rq *rq, struct task_struct *p, int wakeup);
static void dequeue_task_wrr(struct rq *rq, struct task_struct *p, int sleep);
static void requeue_wrr_entity(struct wrr_rq *wrr_rq, struct sched_wrr_entity *wrr_se, int head);
static void requeue_task_wrr(struct rq *rq, struct task_struct *p, int head);
static void yield_task_wrr(struct rq *rq);
static void check_preempt_curr_wrr(struct rq *rq, struct task_struct *p, int flags);
static struct task_struct *pick_next_task_wrr(struct rq *rq);
static void put_prev_task_wrr(struct rq *rq, struct task_struct *p);
static void task_tick_wrr(struct rq *rq, struct task_struct *p, int queued);
unsigned int get_rr_interval_wrr(struct task_struct *task);
static void switched_to_wrr(struct rq *rq, struct task_struct *p, int running);
static void hrtick_wrr_update(struct rq *rq);
static inline struct wrr_rq *wrr_rq_of(struct sched_wrr_entity *se);
static void put_prev_wrr_entity(struct wrr_rq *wrr_rq, struct sched_wrr_entity *prev);

// globals
static const struct sched_class wrr_sched_class;

#define for_each_sched_wrr_entity(se) \
		for (; wrr; wrr = NULL)


static void put_prev_wrr_entity(struct wrr_rq *wrr_rq, struct sched_wrr_entity *prev)
{

	/*
	 * If still on the runqueue then deactivate_task()
	 * was not called and update_curr() has to be done:
	 */
	if (prev->on_rq)
		update_curr(wrr_rq);

	check_spread(wrr_rq, prev);
	if (prev->on_rq) {
		update_stats_wait_start(wrr_rq, prev);
		/* Put 'current' back into the tree. */
		__enqueue_entity(wrr_rq, prev);
	}
	wrr_rq->curr = NULL;

}

static inline struct task_struct *wrr_task_of(struct sched_wrr_entity *se)
{
	return container_of(se, struct task_struct, se);
}

static inline struct wrr_rq *wrr_rq_of(struct sched_wrr_entity *se)
{
	struct task_struct *p = wrr_task_of(se);
	struct rq *rq = task_rq(p);

	return &rq->wrr;
}

static void hrtick_wrr_update(struct rq *rq)
{
        struct task_struct *curr = rq->curr;

        if (curr->sched_class != &wrr_sched_class)
                return;

        if (wrr_rq_of(&curr->wrr)->wrr_nr_running < sched_nr_latency)
                hrtick_start_fair(rq, curr);
}


static inline int on_wrr_rq(struct sched_wrr_entity *wrr_se)
{
	return !list_empty(&wrr_se->run_list);
}


static void enqueue_task_wrr(struct rq *rq, struct task_struct *p, int wakeup)
{
	if (wakeup)
		p->se.start_runtime = p->se.sum_exec_runtime;

	sched_info_queued(p);
	p->sched_class->enqueue_task(rq, p, wakeup);
	p->se.on_rq = 1;
}
#if 0
static void enqueue_task_wrr(struct rq *rq, struct task_struct *p, int wakeup)
{

        struct wrr_rq *wrr_rq;
	struct sched_wrr_entity *wrr = &p->wrr;

	for_each_sched_wrr_entity(wrr) {
		if (wrr->on_rq)
			break;
		wrr_rq = wrr_rq_of(wrr);
		requeue_wrr_entity(wrr_rq, wrr, wakeup);
		wakeup = 1;
	}

	hrtick_wrr_update(rq);
}
#endif

static void dequeue_task_wrr(struct rq *rq, struct task_struct *p, int sleep)
{

	struct wrr_rq *wrr_rq;
	struct sched_wrr_entity *wrr = &p->wrr;

	for_each_sched_wrr_entity(wrr) {
		wrr_rq = wrr_rq_of(wrr);
		requeue_wrr_entity(wrr_rq, wrr, sleep);
		/* Don't dequeue parent if it has other entities besides us */
		if (wrr_rq->wrr_nr_running)
			break;
		sleep = 1;
	}

        hrtick_wrr_update(rq);
}
 #if 0
static void requeue_wrr_entity(struct wrr_rq *wrr_rq, struct sched_wrr_entity *wrr_se, int head)
{

        struct rt_prio_array *array = &rt_rq->active;
        struct list_head *queue = array->queue + rt_se_prio(rt_se);

	if (on_wrr_rq(wrr_se)) {
		if (head)
			list_move(&wrr_se->run_list, queue);
		else
			list_move_tail(&wrr_se->run_list, queue);
	}
}
#endif

static void requeue_task_wrr(struct rq *rq, struct task_struct *p, int head)
{
        struct sched_wrr_entity *wrr_se = &p->wrr;
        struct wrr_rq *wrr_rq;
        wrr_rq = wrr_rq_of(wrr_se);
        requeue_wrr_entity(wrr_rq, wrr_se, head);
}

static void yield_task_wrr(struct rq *rq)
{
        requeue_task_wrr(rq, rq->curr, 0);
}
static void check_preempt_curr_wrr(struct rq *rq, struct task_struct *p, int flags)
{
}

static struct task_struct *pick_next_task_wrr(struct rq *rq)
{
        struct task_struct *p;
        struct wrr_rq *wrr_rq = &rq->wrr;;
        struct sched_wrr_entity *wrr;

	if (unlikely(!wrr_rq->wrr_nr_running))
		return NULL;

	do {
		wrr = pick_next_entity(wrr_rq);
		set_next_entity(wrr_rq, wrr);
		wrr_rq = group_cfs_rq(se);
	} while (cfs_rq);

	p = task_of(wrr);
	hrtick_start_fair(rq, p);

	return p;

}

static void put_prev_task_wrr(struct rq *rq, struct task_struct *p)
{
        struct sched_wrr_entity *wrr = &p->wrr;
        struct wrr_rq *wrr_rq;

        for_each_sched_wrr_entity(wrr) {
                wrr_rq = wrr_rq_of(wrr);
                put_prev_wrr_entity(wrr_rq, wrr);
        }
}

#ifdef CONFIG_SMP
static int select_task_rq_wrr(struct task_struct *p, int sd_flag, int flags)
{
	return task_cpu(p);
}

static unsigned long
load_balance_wrr(struct rq *this_rq, int this_cpu, struct rq *busiest,
                unsigned long max_load_move,
                struct sched_domain *sd, enum cpu_idle_type idle,
                int *all_pinned, int *this_best_prio)
{
        /* don't touch WRR tasks */
        return 0;
}

static int
move_one_task_wrr(struct rq *this_rq, int this_cpu, struct rq *busiest,
                 struct sched_domain *sd, enum cpu_idle_type idle)
{
        return 0;
}


#endif
static void set_curr_task_wrr(struct rq *rq)
{
	struct task_struct *p = rq->curr;

	p->se.exec_start = rq->clock;
}

static void task_tick_wrr(struct rq *rq, struct task_struct *p, int queued)
{
        if (!p->wrr.time_slice) {
                return;
        }
        p->wrr.time_slice = DEF_TIMESLICE * p->wrr.weight;
}

unsigned int get_rr_interval_wrr(struct task_struct *task)
{
        /*
 *          * Time slice is 0 for SCHED_FIFO tasks
 *                   */
        if (task->policy == SCHED_WRR)
                return DEF_TIMESLICE;
        else
                return 0;
}
/* no preemption, so we leave this function empty */
static void prio_changed_wrr(struct rq *rq, struct task_struct *p,
                              int oldprio, int running)
{
}

static void switched_to_wrr(struct rq *rq, struct task_struct *p, int running)
{
        if (running) {
                resched_task(rq->curr);
        } else {
                check_preempt_curr(rq, p, 0);
        }
}

static const struct sched_class wrr_sched_class = {
	.next			= &fair_sched_class,
	.enqueue_task		= enqueue_task_wrr,
	.dequeue_task		= dequeue_task_wrr,
	.yield_task		= yield_task_wrr,

	.check_preempt_curr	= check_preempt_curr_wrr,

	.pick_next_task		= pick_next_task_wrr,
	.put_prev_task		= put_prev_task_wrr,

#ifdef CONFIG_SMP
	.select_task_rq		= select_task_rq_wrr,

	.load_balance		= load_balance_wrr,
	.move_one_task		= move_one_task_wrr,
#endif

	.set_curr_task          = set_curr_task_wrr,
	.task_tick		= task_tick_wrr,

	.get_rr_interval	= get_rr_interval_wrr,

//	.prio_changed		= prio_changed_wrr,
	.switched_to		= switched_to_wrr,
};

