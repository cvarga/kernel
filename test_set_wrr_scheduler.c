#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>

#define __NR_set_wrr_scheduler 342

int main(int argc, char *argv[])
{
        syscall(__NR_set_wrr_scheduler,1,1);
        return 0;
}
