#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define __NR_helloworld  337
#define __NR_print_self  338
#define __NR_print_other 339
#define __NR_get_vstats  340

#define NUM_THREADS 2

void *thread_main(void *arg)
{
	int tid = syscall(SYS_gettid);

	printf("Hello World! It's me, thread #%d!\n", tid);
	syscall(__NR_get_vstats, tid);

	pthread_exit(NULL);
}

int main (int argc, char *argv[])
{
	pthread_t threads[NUM_THREADS];
	int rc, i;
	int pid[NUM_THREADS];
	size_t stack_size;

	pthread_attr_t a;
	pthread_attr_init(&a);
	for(i=0; i<NUM_THREADS; i++){
		pid[i] = i;
		rc = pthread_create(&threads[i], &a, thread_main, (void *) &pid[i]);
		sleep(1);
		if (rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
	}

	/* Last thing that main() should do */
	pthread_exit(NULL);
}
