#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>

#define __NR_get_addr_status  341

int main(int argc, char *argv[])
{
        syscall(__NR_get_addr_status,1);
        return 0;
}
